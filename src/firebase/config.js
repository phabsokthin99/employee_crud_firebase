
import { initializeApp } from 'firebase/app'
import { getFirestore } from 'firebase/firestore'
import { getAuth } from 'firebase/auth'
import {getStorage} from 'firebase/storage'
// For Firebase JS SDK v7.20.0 and later, measurementId is optional

//project save data in sokthinfootball123@gmail.com
const firebaseConfig = {
    apiKey: "AIzaSyDZbmRU0C0fcH_AghTxGeTundyfWRlQY5g",
    authDomain: "savedata-84d9b.firebaseapp.com",
    projectId: "savedata-84d9b",
    storageBucket: "savedata-84d9b.appspot.com",
    messagingSenderId: "1015628570318",
    appId: "1:1015628570318:web:8b273955b0c87d7aa0fa9f",
    measurementId: "G-GFMSKQBNLP"
};

const app = initializeApp(firebaseConfig)
const projectFireStore = getFirestore(app)
const projectAuth = getAuth(app)
const storage = getStorage(app);

export {projectFireStore, projectAuth, storage}
