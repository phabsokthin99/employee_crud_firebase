import { projectFireStore } from "@/firebase/config";
import { addDoc, collection, deleteDoc, doc } from "firebase/firestore";


const useEmployeeCollection = (collectionName) => {

    const collectionRef = collection(projectFireStore, collectionName)

    const addEmployeeDoc = async(formDoc) => {
        try{
            await addDoc(collectionRef, formDoc)
            return true;
        }
        catch(err){
            console.log(err)
        }
    }

    const removeDocs = async(formID) => {
        try{
            await deleteDoc(doc(collectionRef, formID))
        }
        catch(err){
            console.log(err)
        }
    }

    return {addEmployeeDoc, removeDocs}

}

export default useEmployeeCollection;