import { storage } from "@/firebase/config"
import { deleteObject, ref as itemRef } from "firebase/storage"



const useStorage = () => {


    const deleteImage = async(imageUrl) => {

        const storageRef = itemRef(storage, imageUrl)
        try{
            await deleteObject(storageRef)
        }
        catch(err){
            console.log(err)
        }
    }

    return {deleteImage}
}

export default useStorage;