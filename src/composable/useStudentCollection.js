import { projectFireStore } from "@/firebase/config"
import { addDoc, collection, deleteDoc, doc, updateDoc } from "firebase/firestore"
import { ref } from "vue"



const useCollectionStudent =(collectionName) => {

    const isLoading = ref(false)

    const collectionRef = collection(projectFireStore, collectionName)

    const addStudentDocs = async(formDoc) =>{
        try{
            isLoading.value = true
            await addDoc(collectionRef, formDoc)
            isLoading.value = false
            return true
        }
        catch(err){
            console.log(err)
        }
    }

    const deleteStudent = async(docID) => {
  
        try{
            await deleteDoc(doc(collectionRef, docID))
            return true
        }   
        catch(err){
            console.log(err)
        }
    }

    const updateSudent = async(id, formDoc) => {
        isLoading.value = true
        const documentsRef = doc(collectionRef, id)
        try{
            await updateDoc(documentsRef, formDoc)
            isLoading.value = false
            return true
        }
        catch(err){
            console.log(err)
        }
    }
  
    return {isLoading, addStudentDocs, deleteStudent, updateSudent}
}

export default useCollectionStudent;