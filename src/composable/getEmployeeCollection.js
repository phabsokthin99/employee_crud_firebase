import { projectFireStore } from "@/firebase/config";
import { collection, onSnapshot, query } from "firebase/firestore";
import { ref, watchEffect } from "vue";


const getEmployeeCollection = (collectionName) => {

    const documents = ref(null)
    try{
        const collectionRef = query(collection(projectFireStore, collectionName))
        const unsubscripe = onSnapshot(collectionRef, (qry)=>{
            const result = []
            qry.forEach((doc)=>{
                result.push({id: doc.id, ...doc.data()})
            })
    
            documents.value = result
        })
        watchEffect((onInvalidate)=>{
            onInvalidate(()=>unsubscripe())
        })
    }
    catch(err){
        console.log(err)
    }

    return {documents}

}

export default getEmployeeCollection;