import { projectFireStore } from "@/firebase/config"
import { collection, onSnapshot, query } from "firebase/firestore"
import { ref, watchEffect } from "vue"


const getStudentCollection = (collectionName) => {  

    const documents = ref(null)
    const collectionRef = query(collection(projectFireStore, collectionName))

    const unsubscripe = onSnapshot(collectionRef, (qry) => {
        const result = []
        qry.forEach((doc) => {
            result.push({id: doc.id, ...doc.data()})
            documents.value = result
        }, (err) =>{
            console.log(err)
        })
      
    })

    watchEffect((onInvalidate) => {
        onInvalidate(() => unsubscripe())
    })

    return {documents}
}

export default getStudentCollection;