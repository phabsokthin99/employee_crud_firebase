import { createRouter, createWebHistory } from 'vue-router'
import EmployeeView from '../view/EmployeeView.vue'
import StudentView from '@/view/StudentView.vue'
import EditView from '@/view/EditView.vue'
const routes = [
  {
    path: '/',
    name: 'student',
    component: StudentView
  },
  {
    path: '/emp',
    name: 'emp',
    component: EmployeeView
  },
  {
    path: "/emp_edit/:id",
    name: "emp_edit",
    component: EditView
  }
]

const router = createRouter({
  history: createWebHistory(process.env.BASE_URL),
  routes
})

export default router
